package com.agero.ncc.driver.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.ActiveJobsAdapter;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.firestore.FirestoreJobData;
import com.agero.ncc.fragments.AddJobsDetailsFragment;
import com.agero.ncc.fragments.BaseFragment;
import com.agero.ncc.model.Equipment;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.RecyclerItemClickListener;
import com.apptentive.android.sdk.Apptentive;
import com.google.android.gms.location.LocationRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import timber.log.Timber;


public class ActiveJobsFragment extends BaseFragment implements HomeActivity.OnDutyToolbarListener {

    @BindView(R.id.constarint_settings)
    ConstraintLayout mConstarintSettings;
    @BindView(R.id.recycler_joblist)
    RecyclerView mRecyclerJoblist;
    @BindView(R.id.text_lable_settings)
    TextView textLableSettings;
    @BindView(R.id.text_settings)
    TextView textSettings;
    @BindView(R.id.fab_add)
    FloatingActionButton mfabAdd;
    String mDispatchNumber;
    boolean isFromChat;
    String selectedDispatchNumber;
    @BindView(R.id.text_activejobs_message)
    TextView mTextActivejobsMessage;
    @BindView(R.id.text_activejobs_ok)
    TextView mTextActivejobsOk;
    @BindView(R.id.card_view_active_job_duty)
    CardView mCardViewActiveJobHint;

    HomeActivity mHomeActivity;
    @BindView(R.id.constraint_active_jobs)
    ConstraintLayout mConstraintActiveJobs;
    @BindView(R.id.cons_activejobs)
    ConstraintLayout mConsActivejobs;
    UserError mUserError;
    private ActiveJobsAdapter jobListAdapter;
    private ArrayList<JobDetail> jobList;
    private DatabaseReference myRef, mDriverRef;
    private long oldRetryTime = 0;
    private int serviceUnFinished = 0;
    int selectedJobPosition;

    String eventAction = "";
    String eventCategory = NccConstants.FirebaseEventCategory.JOBS;

    ValueEventListener valueEventDriverListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            hideProgress();
            if (isAdded()) {
                loadDriverList(dataSnapshot);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Driver Active Jobs User Equipment Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getDriverDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };
    private boolean mIsOnDuty = false;
    private ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (isAdded()) {
                jobList.clear();
                if (dataSnapshot.hasChildren()) {
                    Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
                    String logEventText = "";

                   /* JobDetail currentJobInGeofence = null;
                    if(SparkLocationUpdateService.jobDataForGeofenceArrayList != null && !SparkLocationUpdateService.jobDataForGeofenceArrayList.isEmpty() && SparkLocationUpdateService.jobDataForGeofenceArrayList.size() >0){
                        currentJobInGeofence = SparkLocationUpdateService.jobDataForGeofenceArrayList.get(0);
                    }*/

//                    SparkLocationUpdateService.clearList();

                    for (DataSnapshot data : childList) {
                        try {
                            JobDetail job = data.getValue(JobDetail.class);

                            if (job.getPartnerFacility() != null && !TextUtils.isEmpty(job.getPartnerFacility().getPhoneNumber())) {
                                mEditor.putString(NccConstants.DISPATCHER_NUMBER, job.getPartnerFacility().getPhoneNumber()).commit();
                            }
                            Gson gson = new Gson();
                            Timber.d(job.getDispatchId() + " " + gson.toJson(job));
                            if (job.getDispatchId() != null) {
                                if(NccConstants.IS_GEOFENCE_ENABLED) {
                                    SparkLocationUpdateService.checkJobToAddOrRemove(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), job);
                                }
                                jobList.add(job);

                                /*if(currentJobInGeofence != null && job.getDispatchId().equalsIgnoreCase(currentJobInGeofence.getDispatchId())){
                                    if(job.getCurrentStatus() != null && !TextUtils.isEmpty(job.getCurrentStatus().getStatusCode()) && !job.getCurrentStatus().getStatusCode().equalsIgnoreCase(currentJobInGeofence.getCurrentStatus().getStatusCode())) {
                                        SparkLocationUpdateService.jobDataForGeofenceArrayList.set(0, job);
                                    }
                                }*/
                            }
                            if (!logEventText.isEmpty()) {
                                logEventText += ", ";
                            }
                            logEventText += job.getDispatchId();
                        } catch (Exception e) {
                            mHomeActivity.mintLogException(e);
                            e.printStackTrace();
                        }
                    }
                    HashMap<String, String> extraDatas = new HashMap<>();
                    extraDatas.put("jobs", logEventText);
                    mHomeActivity.mintlogEventExtraData("Job List", extraDatas);
                    Collections.sort(jobList, new Comparator<JobDetail>() {
                        public int compare(JobDetail job1, JobDetail job2) {

                            try {
                                Date date1 = DateTimeUtils.parse(job1.getEtaDateTime());
                                Date date2 = DateTimeUtils.parse(job2.getEtaDateTime());
                                return date1.compareTo(date2);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            return job1.getEtaDateTime().compareToIgnoreCase(job2.getEtaDateTime()); // To compare string values

                        }
                    });
                }

                if (mIsOnDuty) {
                    mCardViewActiveJobHint.setVisibility(View.GONE);
                    mTextActivejobsMessage.setVisibility(View.VISIBLE);
                    if (jobList.size() > 0) {
                        mRecyclerJoblist.setVisibility(View.VISIBLE);
                        mTextActivejobsMessage.setVisibility(View.GONE);
                    } else {
                        mRecyclerJoblist.setVisibility(View.GONE);
                        if (getResources().getBoolean(R.bool.isTablet)) {
                        }
                        mTextActivejobsMessage.setText(Html.fromHtml(getString(R.string.activejobs_message)));
                    }
                } else {
                    mTextActivejobsMessage.setText(Html.fromHtml(getString(R.string.offduty_relax_message)));
                    jobList.clear();
                    if (!mPrefs.getBoolean(NccConstants.IS_OKGOTIT, false)) {
                        mCardViewActiveJobHint.setVisibility(View.VISIBLE);
                    }
                }

                jobListAdapter.notifyDataSetChanged();

                if (jobList != null && jobList.size() > 0) {
                    if (selectedDispatchNumber == null || TextUtils.isEmpty(selectedDispatchNumber)) {
                        for (int i = 0; i < jobList.size(); i++) {
                            if (jobList.get(i).getDispatchId() != null) {
                                selectedDispatchNumber = jobList.get(i).getDispatchId();
                                selectedJobPosition = i;
                                break;
                            }
                        }
                    }
                    jobListAdapter = new ActiveJobsAdapter(getActivity(), jobList, mPrefs,selectedJobPosition);
                    mRecyclerJoblist.setAdapter(jobListAdapter);

                    if (!TextUtils.isEmpty(selectedDispatchNumber)){
                        if (getResources().getBoolean(R.bool.isTablet)) {
                            FirestoreJobData.getInStance().setFirestore(false);
                            mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, false));
                        }
                    }

                    /*if (!TextUtils.isEmpty(selectedDispatchNumber) && getResources().getBoolean(R.bool.isTablet)) {
//                        mViewSeparator.setVisibility(View.VISIBLE);
//                        FragmentTransaction ft = getFragmentManager()
//                                .beginTransaction();
//                        ft.replace(R.id.frame_job_details, JobDetailsFragment.newInstance(selectedDispatchNumber, false), getString(R.string.title_assigned));
////                        ft.addToBackStack(getString(R.string.job_status_assigned));
//                        ft.commit();
                        mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, false));
                    }*/
                }

                hideProgress();

                mEditor.putInt("jobsize", jobList.size()).commit();

                if (jobList.size() == 0) {
                    mHomeActivity.hideRightFrame();
                    mHomeActivity.showToolbar(getString(R.string.toolbar_text_duty));
                }


            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Driver Active Jobs List Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getJobListDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    public ActiveJobsFragment() {
        // Intentionally empty
    }

    public static ActiveJobsFragment newInstance() {
        ActiveJobsFragment fragment = new ActiveJobsFragment();
//        fragment.selectedDispatchNumber = "";
//        fragment.selectedJobPosition = 0;
        return fragment;
    }

    public static ActiveJobsFragment newInstance(String dispatchNumber, boolean isFromChat) {
        ActiveJobsFragment fragment = new ActiveJobsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.JOB_ID, dispatchNumber);
        bundle.putBoolean(NccConstants.IS_FROM_CHAT, isFromChat);
        fragment.setArguments(bundle);
        //fragment.selectedDispatchNumber = dispatchNumber;
        //fragment.isFromChat = isFromChat;
        //fragment.selectedJobPosition = 0;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        final View view = inflater.inflate(R.layout.fragment_activejobs, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        mHomeActivity = (HomeActivity) getActivity();
        Apptentive.engage(getActivity(), NccConstants.APPTENTIVE_JOBS);
        mUserError = new UserError();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerJoblist.setLayoutManager(linearLayoutManager);
        mRecyclerJoblist.removeItemDecoration(new DividerItemDecoration(getActivity(), linearLayoutManager.getOrientation()));
        jobList = new ArrayList<>();

        jobListAdapter = new ActiveJobsAdapter(getActivity(), jobList, mPrefs,selectedJobPosition);
        mRecyclerJoblist.setAdapter(jobListAdapter);

        mRecyclerJoblist.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        eventAction = NccConstants.FirebaseEventAction.VIEW_JOB_DETAIL;
                        if(isAdded() && position != -1 && jobList.size() > position) {

                            mDispatchNumber = jobList.get(position).getDispatchId();
                            selectedDispatchNumber = mDispatchNumber;
                            selectedJobPosition = position;
                            FirestoreJobData.getInStance().setFirestore(false);
                            if (getResources().getBoolean(R.bool.isTablet)) {
                                mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, false));
                            } else {
                                if (position >= 0) {
                                    mHomeActivity.push(JobDetailsFragment.newInstance(mDispatchNumber, false),
                                            getString(R.string.title_assigned));
                                }

                            }
                        }
                    }
                }));
        mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
        mTextActivejobsMessage.setVisibility(View.GONE);
        mCardViewActiveJobHint.setVisibility(View.GONE);

        setOffDuty();
        if (Utils.isNetworkAvailable()) {

            if(getArguments()!=null){
                selectedDispatchNumber = getArguments().getString(NccConstants.JOB_ID);
                isFromChat = getArguments().getBoolean(NccConstants.IS_FROM_CHAT);
            }

            if (!getResources().getBoolean(R.bool.isTablet) && !TextUtils.isEmpty(selectedDispatchNumber) && isFromChat) {
                FirestoreJobData.getInStance().setFirestore(false);
                mHomeActivity.push(JobDetailsFragment.newInstance(selectedDispatchNumber, false) , getString(R.string.title_jobdetail));
                getArguments().remove(NccConstants.JOB_ID);
                getArguments().remove(NccConstants.IS_FROM_CHAT);
                isFromChat = false;
            }else {
                getDriverDataFromFirebase();
                if (!mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "").isEmpty()
                        && !mPrefs.getString(NccConstants.SIGNIN_USER_ID, "").isEmpty()) {
                    getJobListDataFromFirebase();
                }
            }

            setStausChangeEventListener();
            mHomeActivity.setBGClickableFalse();
            mHomeActivity.registerFCM();

        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

        return superView;
    }

    private void getJobListDataFromFirebase() {
        showProgress();
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    String vendorId = mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "");
                    String userId = mPrefs.getString(NccConstants.SIGNIN_USER_ID, "");
                    if (myRef != null && !TextUtils.isEmpty(vendorId) && !TextUtils.isEmpty(userId)) {
                        myRef.child(vendorId).orderByChild("dispatchAssignedToId").equalTo(userId).addValueEventListener(valueEventListener);
                    }
                }
            }

            @Override
            public void onRefreshFailure() {
                if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    private void getDriverDataFromFirebase() {
        showProgress();
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    myRef = database.getReference("ActiveJobs/");
                    myRef.keepSynced(true);
                    mDriverRef = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/"
                            + mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                    mDriverRef.keepSynced(true);
                    mDriverRef.child("Equipment").addValueEventListener(valueEventDriverListener);
                }
            }

            @Override
            public void onRefreshFailure() {
                if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    private void getDutyJobListDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        if (myRef != null && (!mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "").isEmpty() && !mPrefs.getString(NccConstants.SIGNIN_USER_ID, "").isEmpty())) {
                            myRef.child(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")).orderByChild("dispatchAssignedToId").equalTo(mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")).addValueEventListener(valueEventListener);
                        }
                    }
                }

                @Override
                public void onRefreshFailure() {
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isPermissionDenied()) {
            mConstarintSettings.setVisibility(View.VISIBLE);
        } else {
            mConstarintSettings.setVisibility(View.GONE);
        }

        mHomeActivity.showBottomBar();
        mHomeActivity.setOnDutyToolbarListener(this);
//        if(getResources().getBoolean(R.bool.isTablet) && isAdded() && isVisible()) {
        mHomeActivity.showToolbar(getString(R.string.toolbar_text_duty));
//        }else if(!getResources().getBoolean(R.bool.isTablet)){
//            mHomeActivity.showToolbar(getString(R.string.toolbar_text_duty));
//        }
        mTextActivejobsMessage.setVisibility(View.GONE);
        mCardViewActiveJobHint.setVisibility(View.GONE);
    }

    @Override
    public void onDutyCall() {
        mHomeActivity.getLocationUpdates();
    }

    @Override
    public void onTextDuty() {
        mHomeActivity.setBGClickableFalse();
    }

    @Override
    public void offDutyCall() {
        mHomeActivity.stopLocationUpdates();
    }

    @Override
    public void showProgress() {
        if (serviceUnFinished <= 0) {
            super.showProgress();
        }
        serviceUnFinished++;
    }

    @Override
    public void hideProgress() {
        serviceUnFinished--;
        if (serviceUnFinished <= 0) {
            super.hideProgress();
        }
    }

    private void setStausChangeEventListener() {
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    mDriverRef = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/"
                            + mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));

                    RxFirebaseDatabase.observeValueEvent(mDriverRef.child("onDuty"), Boolean.class).subscribe(taskSnapshot -> {
                        if (taskSnapshot != null) {
                            if (!taskSnapshot) {
                                mHomeActivity.setDuty(getString(R.string.title_offduty));

                                HashMap<String, String> extraDatas = new HashMap<>();
                                extraDatas.put("json", getString(R.string.title_offduty));
                                mHomeActivity.mintlogEventExtraData("Active Jobs Duty", extraDatas);

                                mTextActivejobsMessage.setVisibility(View.VISIBLE);
                                setOffDuty();
                                if (mHomeActivity.isOffDuty() && !mPrefs.getBoolean(NccConstants.IS_OKGOTIT, false)) {
                                    mCardViewActiveJobHint.setVisibility(View.VISIBLE);
                                }

                                SparkLocationUpdateService.onDutyFlag = false;
                                mHomeActivity.stopSparkLocationService();

                            } else {
                                mHomeActivity.setDuty(getString(R.string.title_onduty));
                                HashMap<String, String> extraDatas = new HashMap<>();
                                extraDatas.put("json", getString(R.string.title_onduty));
                                mHomeActivity.mintlogEventExtraData("Active Jobs Duty", extraDatas);
                                setOnDuty();

                                SparkLocationUpdateService.onDutyFlag = true;
                                mHomeActivity.startSparkLocationService();
                            }
                        }

                    }, throwable -> {
                        if (isAdded()) {
                            mHomeActivity.mintlogEvent("Driver Active Jobs Duty Change Firebase Database Error - " + throwable.getLocalizedMessage());
                            if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                                setStausChangeEventListener();
                                oldRetryTime = System.currentTimeMillis();
                            }
                        }
                    });
                }
            }

            @Override
            public void onRefreshFailure() {
                if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    private void setOffDuty() {
        if (myRef != null)
            myRef.removeEventListener(valueEventListener);
        if (jobList != null)
            jobList.clear();
        if (jobListAdapter != null)
            jobListAdapter.notifyDataSetChanged();
        mIsOnDuty = false;
        mTextActivejobsMessage.setText(Html.fromHtml(getString(R.string.offduty_relax_message)));
        setJobStatusUI(getString(R.string.title_offduty));
//        if (mHomeActivity.isOffDuty() && !mPrefs.getBoolean(NccConstants.IS_OKGOTIT, false)) {
//                mCardViewActiveJobHint.setVisibility(View.VISIBLE);
//            }
//        SparkLocationUpdateService.onDutyFlag = mIsOnDuty;
//        mHomeActivity.stopSparkLocationService();
    }

    private void setOnDuty() {
        mIsOnDuty = true;
        getDutyJobListDataFromFirebase();
        setJobStatusUI(getString(R.string.title_onduty));
//        SparkLocationUpdateService.onDutyFlag = mIsOnDuty;
//        mHomeActivity.startSparkLocationService();
    }

    public boolean isPermissionDenied() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return
                    ContextCompat.checkSelfPermission(mHomeActivity.getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_DENIED || !Utils.isGpsEnabled(getActivity());
        } else {
            return false;
        }
    }

    private void loadDriverList(DataSnapshot dataSnapshot) {
        if (dataSnapshot != null) {
            try {
                Equipment equipment = dataSnapshot.getValue(Equipment.class);
                if (equipment != null) {
                    String json = new Gson().toJson(equipment);
                    HashMap<String, String> extraDatas = new HashMap<>();
                    extraDatas.put("json", json);
                    mHomeActivity.mintlogEventExtraData("Active Jobs Equipment Json", extraDatas);
                    mEditor.putString(NccConstants.EQUIPMENT_PREF_KEY, json).commit();
                    mEditor.putString(NccConstants.EQUIPMENT_ID, equipment.getEquipmentId()).commit();
                } else {
                    mEditor.putString(NccConstants.EQUIPMENT_PREF_KEY, new Gson().toJson("")).commit();
                    mEditor.putString(NccConstants.EQUIPMENT_ID, "-1").commit();
                }
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
                e.printStackTrace();
            }
        }
    }

    private void setJobStatusUI(String duty) {
        mCardViewActiveJobHint.setVisibility(View.GONE);
        mTextActivejobsMessage.setVisibility(View.GONE);
        if (duty.equalsIgnoreCase("On Duty")) {
            mTextActivejobsMessage.setText(Html.fromHtml(getString(R.string.activejobs_message)));
            if (jobList.size() > 0 || mPrefs.getBoolean(NccConstants.IS_OKGOTIT, false)) {
                mTextActivejobsMessage.setVisibility(View.GONE);
                mCardViewActiveJobHint.setVisibility(View.GONE);
            }
        } else {
            mTextActivejobsMessage.setVisibility(View.VISIBLE);
            mTextActivejobsMessage.setText(Html.fromHtml(getString(R.string.offduty_relax_message)));
            if (mPrefs.getBoolean(NccConstants.IS_OKGOTIT, false)) {
                mCardViewActiveJobHint.setVisibility(View.GONE);
            }

        }
    }

    @OnClick({R.id.text_settings, R.id.fab_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_settings:
                Utils.gotoSettings(getActivity());
                break;
            case R.id.fab_add:
                mHomeActivity.push(AddJobsDetailsFragment.newInstance(true), getString(R.string.title_add_job));
                break;
        }
    }

    @OnClick(R.id.text_activejobs_ok)
    public void onViewClicked() {
        mEditor.putBoolean(NccConstants.IS_OKGOTIT, true).commit();
        mCardViewActiveJobHint.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        removeValueEventListeners();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        removeValueEventListeners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeValueEventListeners();
    }

    private void removeValueEventListeners() {
//        if(mDutyJobReference != null) {
//            mDutyJobReference.removeEventListener(valueEventListener);
//        }
        try {
            if (mDriverRef != null) {
                mDriverRef.removeEventListener(valueEventDriverListener);
            }
        }catch (Exception e){

        }

//        if(mJobReference != null) {
//            mJobReference.removeEventListener(valueEventListener);
//        }
    }

}
