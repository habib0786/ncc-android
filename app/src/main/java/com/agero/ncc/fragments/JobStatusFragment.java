package com.agero.ncc.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.firestore.FirestoreJobData;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Profile;
import com.agero.ncc.model.SparkLocationData;
import com.agero.ncc.model.Status;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.JobStatusView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import durdinapps.rxfirebase2.RxFirebaseDatabase;

import static com.agero.ncc.services.SparkLocationUpdateService.GEOFENCE_DISTANCE;
import static com.agero.ncc.utils.NccConstants.*;

public class JobStatusFragment extends BaseStatusFragment implements HomeActivity.ToolbarSaveListener {

    private boolean isItFromHistory;
    private boolean isTow;
    String statusDescription = null;
    HomeActivity mHomeActivity;
    DatabaseReference currentStatusDatabaseReference;
    DatabaseReference statusHistoryDatabaseReference;
    DatabaseReference jobDatabaseReference;
    @BindView(R.id.text_select_current_status)
    TextView mTextSelectCurrentStatus;
    @BindView(R.id.view_border_current_status)
    View mViewBorderCurrentStatus;
    @BindView(R.id.job_status_view_assigned)
    JobStatusView mJobStatusViewAssigned;
    @BindView(R.id.job_status_view_en_route)
    JobStatusView mJobStatusViewEnRoute;
    @BindView(R.id.job_status_view_on_scene)
    JobStatusView mJobStatusViewOnScene;
    @BindView(R.id.job_status_view_tow_progress)
    JobStatusView mJobStatusViewTowProgress;
    @BindView(R.id.job_status_view_destination_arrived)
    JobStatusView mJobStatusViewDestinationArrived;
    @BindView(R.id.job_status_view_job_completed)
    JobStatusView mJobStatusViewJobCompleted;
    @BindView(R.id.text_job_status_job_hold)
    TextView mTextJobStatusJobHold;
    @BindView(R.id.switch_job_hold)
    Switch mSwitchJobHold;
    @BindView(R.id.text_job_reason_place_job_on_hold)
    TextView mTextReasonJobOnHold;
    @BindView(R.id.text_job_reason_description)
    TextView mTextReasonDescription;
    @BindView(R.id.constraint_job_hold)
    ConstraintLayout mConstraintJobHold;
    @BindView(R.id.view_border_tow_complete_status)
    View mViewBorderComplete;
    @BindView(R.id.view_border_job_status_description)
    View mViewBorderStatusDescription;
    @BindView(R.id.view_border_tow_status)
    View mViewBorderTowStatus;
    @BindView(R.id.view_border_tow_destination_status)
    View mViewBorderTowDesstinationStatus;
    String mDispatchNumber, mTimeZone, mDispatchAssignId, mDispatchAssignName, mDriverAssignedById, mDriverAssignedByName;
    UserError mUserError;
    private Status currentStatus;
    private HashMap<String, String> statusHistoryKey;
    private int serviceUnFinished = 0;
    private long oldRetryTime = 0;
    Status statusHistory;
    ArrayList<Status> statusHistoryList;
    private boolean isEditable;
    private boolean isClickable = true;
    private JobDetail mCurrentJob;

    public static JobStatusFragment newInstance(String sectionNumber, boolean isItFromHistory, boolean isTow, boolean isEditable, String timeZone, String dispatchAssignId, String dispatchAssignName, String driverAssignedById, String driverAssignedByName, JobDetail currentJob) {
        JobStatusFragment fragment = new JobStatusFragment();
        Bundle args = new Bundle();
        args.putString(NccConstants.DISPATCH_REQUEST_NUMBER, sectionNumber);
        args.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isItFromHistory);
        args.putBoolean(NccConstants.IS_EDITABLE, isEditable);
        args.putBoolean(NccConstants.IS_TOW, isTow);
        args.putString(NccConstants.TIME_ZONE, timeZone);
        args.putString(NccConstants.DISPATCH_ASSIGNED_ID, dispatchAssignId);
        args.putString(NccConstants.DISPATCH_ASSIGNED_NAME, dispatchAssignName);
        args.putString(NccConstants.DRIVER_ASSIGNED_BY_ID, driverAssignedById);
        args.putString(NccConstants.DRIVER_ASSIGNED_BY_NAME, driverAssignedByName);
        args.putString(NccConstants.JOB_DETAIL, new Gson().toJson(currentJob));
        fragment.setArguments(args);
        return fragment;
    }

    public JobStatusFragment() {
        // Intentionally empty
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_job_status, fragment_content, true);
        ButterKnife.bind(this, view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mDispatchNumber = bundle.getString(NccConstants.DISPATCH_REQUEST_NUMBER);
            isItFromHistory = bundle.getBoolean(NccConstants.IS_IT_FROM_HISTORY);
            isEditable = bundle.getBoolean(NccConstants.IS_EDITABLE);
            isTow = bundle.getBoolean(NccConstants.IS_TOW);
            mTimeZone = bundle.getString(NccConstants.TIME_ZONE);
            mDispatchAssignId = bundle.getString(NccConstants.DISPATCH_ASSIGNED_ID);
            mDispatchAssignName = bundle.getString(NccConstants.DISPATCH_ASSIGNED_NAME);
            mDriverAssignedById = bundle.getString(NccConstants.DRIVER_ASSIGNED_BY_ID);
            mDriverAssignedByName = bundle.getString(NccConstants.DRIVER_ASSIGNED_BY_NAME);
            mCurrentJob = new Gson().fromJson(getArguments().getString(NccConstants.JOB_DETAIL), JobDetail.class);
        }

        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        if (!isEditable) {
            mHomeActivity.showToolbar(getString(R.string.title_job_status));
            mJobStatusViewJobCompleted.setStatusText(getResources().getString(R.string.history_text_completed));
            mJobStatusViewJobCompleted.setVisibility(View.VISIBLE);
            mViewBorderComplete.setVisibility(View.VISIBLE);
        } else {
            mHomeActivity.showToolbar(getString(R.string.title_job_modify_status));
            mJobStatusViewJobCompleted.setStatusText(JOB_STATUS_JOB_COMPLETED);
            mJobStatusViewJobCompleted.setVisibility(View.VISIBLE);
            mViewBorderComplete.setVisibility(View.VISIBLE);
        }
        mHomeActivity.setOnToolbarSaveListener(this);
        mHomeActivity.hideSaveButton();

        mJobStatusViewAssigned.setStatusText(JOB_STATUS_ASSIGNED);
        mJobStatusViewEnRoute.setStatusText(JOB_STATUS_EN_ROUTE);
        mJobStatusViewOnScene.setStatusText(JOB_STATUS_ON_SCENE);
        mJobStatusViewTowProgress.setStatusText(JOB_STATUS_TOWING);
        mJobStatusViewDestinationArrived.setStatusText(JOB_STATUS_DESTINATION_ARRIVAL);
        mUserError = new UserError();

        if (FirestoreJobData.getInStance().isFirestore()) {
            mCurrentJob = FirestoreJobData.getInStance().getJobDetail();
            for (Status data : mCurrentJob.getStatusHistory()) {
                loadStatus(data);
            }
            currentStatus = mCurrentJob.getCurrentStatus();
            loadCurrentStatus();
            updateCurrentStatusUI(currentStatus.getStatusCode(), currentStatus.getServerTimeUtc(), currentStatus.getIsGeofence() ? "GPS Update" : currentStatus.getUserName());
        } else {
            if (Utils.isNetworkAvailable()) {
                getStatusHistoryDataFromFirebase();
            } else {
                mUserError.title = "";
                mUserError.message = getString(R.string.network_error_message);
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            }
        }


        return superView;
    }

    private void getStatusHistoryDataFromFirebase() {

        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    String jobsPath;
                    if (isItFromHistory) {
                        jobsPath = "InActiveJobs/";
                    } else {
                        jobsPath = "ActiveJobs/";
                    }
                    serviceUnFinished++;
                    statusHistoryDatabaseReference = database.getReference(jobsPath + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber + "/statusHistory");
                    //statusHistoryDatabaseReference.keepSynced(true);
                    statusHistoryDatabaseReference.addValueEventListener(statusHistoryEventListener);
                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });

    }

    private void getStatusDataFromFirebase() {
        showProgress();
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {

                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    String jobsPath;
                    if (isItFromHistory) {
                        jobsPath = "InActiveJobs/";
                    } else {
                        jobsPath = "ActiveJobs/";
                    }
                    serviceUnFinished++;
                    jobDatabaseReference = database.getReference(jobsPath + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber);
                    // jobDatabaseReference.keepSynced(true);
                    currentStatusDatabaseReference = database.getReference(jobsPath + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber + "/currentStatus");
                    //currentStatusDatabaseReference.keepSynced(true);
                    currentStatusDatabaseReference.addValueEventListener(currentStatusEventListener);


                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    ValueEventListener currentStatusEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            serviceUnFinished--;
            if (serviceUnFinished <= 0) {
                hideProgress();
            }
            if (dataSnapshot.hasChildren() && isAdded()) {
                try {
                    currentStatus = dataSnapshot.getValue(Status.class);

                    if (currentStatus != null) {
                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("dispatch_id", mDispatchNumber);
                        extraDatas.put("json", new Gson().toJson(currentStatus));
                        mHomeActivity.mintlogEventExtraData("Job Status Current Status", extraDatas);

                        mCurrentJob.setCurrentStatus(currentStatus);
                        if (NccConstants.IS_GEOFENCE_ENABLED) {
                            SparkLocationUpdateService.checkJobToAddOrRemove(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), mCurrentJob);
                        }
                    }

                    updateCurrentStatusUI(currentStatus.getStatusCode(), currentStatus.getServerTimeUtc(), currentStatus.getIsGeofence() ? "GPS Update" : currentStatus.getUserName());
                } catch (Exception e) {
                    mHomeActivity.mintLogException(e);
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Job Status Current Status Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getStatusDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private void updateCurrentStatusUI(String currentStatusCode, String serverTimeUtc, String userName) {
        if (currentStatus != null && currentStatus.getIsJobOnHold() != null && currentStatus.getIsJobOnHold()) {
            mSwitchJobHold.setChecked(true);

            enableDisableStaus(currentStatus.getIsJobOnHold());
        } else {
            mSwitchJobHold.setChecked(false);
            mTextReasonJobOnHold.setVisibility(View.GONE);
            mTextReasonDescription.setVisibility(View.GONE);
            mViewBorderStatusDescription.setVisibility(View.GONE);
        }
        updateUiStatus(currentStatusCode, serverTimeUtc, userName);
        if (!isEditable) {
            updateUiForHistory();
        }
    }

    ValueEventListener statusHistoryEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            serviceUnFinished--;
            if (serviceUnFinished <= 0) {
                hideProgress();
            }

            statusHistoryKey = new HashMap<>();
            if (dataSnapshot.hasChildren() && isAdded()) {
                Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
                statusHistoryList = new ArrayList<>();
                for (DataSnapshot data : childList) {
                    try {
                        statusHistory = data.getValue(Status.class);
                        statusHistoryList.add(data.getValue(Status.class));
                        loadStatus(statusHistory);
                    } catch (Exception e) {
                        mHomeActivity.mintLogException(e);
                    }
                }
                loadCurrentStatus();
                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("dispatch_id", mDispatchNumber);
                extraDatas.put("json", new Gson().toJson(statusHistoryList));
                mHomeActivity.mintlogEventExtraData("Job Status Status History", extraDatas);
                mCurrentJob.setStatusHistory(statusHistoryList);
            }


            getStatusDataFromFirebase();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Job Status Status History Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getStatusHistoryDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private void loadCurrentStatus() {
        if (currentStatus != null && currentStatus.getStatusCode() != null && JOB_STATUS_JOB_COMPLETED.equalsIgnoreCase(currentStatus.getStatusCode()) && !currentStatus.getDeleted()) {
            //statusHistoryKey.put(JOB_STATUS_JOB_COMPLETED, data.getKey());

            try {
                try {
                    mJobStatusViewJobCompleted.setStatusDescription(DateTimeUtils.getDisplayJobStatus(
                            DateTimeUtils.parse(currentStatus.getServerTimeUtc()), mTimeZone).trim() + " "
                            + getString(R.string.dot) + " " + currentStatus.getUserName());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } catch (StringIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadStatus(Status statusHistory) {
        try {
            if (!statusHistory.getDeleted()) {
                if (JOB_STATUS_ASSIGNED.equalsIgnoreCase(statusHistory.getStatusCode())) {
                    try {
                        mJobStatusViewAssigned.setStatusDescription(DateTimeUtils.getDisplayJobStatus(
                                DateTimeUtils.parse(statusHistory.getServerTimeUtc()), mTimeZone).trim() + " "
                                + getString(R.string.dot) + " " + statusHistory.getUserName());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (JOB_STATUS_EN_ROUTE.equalsIgnoreCase(statusHistory.getStatusCode()) && !statusHistory.getDeleted()) {
                    //statusHistoryKey.put(JOB_STATUS_EN_ROUTE, data.getKey());
                    try {
                        mJobStatusViewEnRoute.setStatusDescription(DateTimeUtils.getDisplayJobStatus(
                                DateTimeUtils.parse(statusHistory.getServerTimeUtc()), mTimeZone).trim() + " "
                                + getString(R.string.dot) + " " + statusHistory.getUserName());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (JOB_STATUS_ON_SCENE.equalsIgnoreCase(statusHistory.getStatusCode()) && !statusHistory.getDeleted()) {
                    //statusHistoryKey.put(JOB_STATUS_ON_SCENE, data.getKey());
                    try {
                        if (statusHistory.getIsGeofence()) {
                            mJobStatusViewOnScene.setStatusDescription(DateTimeUtils.getDisplayJobStatus(
                                    DateTimeUtils.parse(statusHistory.getServerTimeUtc()), mTimeZone).trim() + " "
                                    + getString(R.string.dot) + " GPS Update");
                        } else {
                            mJobStatusViewOnScene.setStatusDescription(DateTimeUtils.getDisplayJobStatus(
                                    DateTimeUtils.parse(statusHistory.getServerTimeUtc()), mTimeZone).trim() + " "
                                    + getString(R.string.dot) + " " + statusHistory.getUserName());
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (JOB_STATUS_TOWING.equalsIgnoreCase(statusHistory.getStatusCode()) && !statusHistory.getDeleted()) {
                    //statusHistoryKey.put(JOB_STATUS_TOWING, data.getKey());
                    try {
                        if (statusHistory.getIsGeofence()) {
                            mJobStatusViewTowProgress.setStatusDescription(DateTimeUtils.getDisplayJobStatus(
                                    DateTimeUtils.parse(statusHistory.getServerTimeUtc()), mTimeZone).trim() + " "
                                    + getString(R.string.dot) + " GPS Update");
                        } else {
                            mJobStatusViewTowProgress.setStatusDescription(DateTimeUtils.getDisplayJobStatus(
                                    DateTimeUtils.parse(statusHistory.getServerTimeUtc()), mTimeZone).trim() + " "
                                    + getString(R.string.dot) + " " + statusHistory.getUserName());
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (JOB_STATUS_DESTINATION_ARRIVAL.equalsIgnoreCase(statusHistory.getStatusCode()) && !statusHistory.getDeleted()) {
                    //statusHistoryKey.put(JOB_STATUS_DESTINATION_ARRIVAL, data.getKey());
                    try {
                        if (statusHistory.getIsGeofence()) {
                            mJobStatusViewDestinationArrived.setStatusDescription(DateTimeUtils.getDisplayJobStatus(
                                    DateTimeUtils.parse(statusHistory.getServerTimeUtc()), mTimeZone).trim() + " "
                                    + getString(R.string.dot) + " GPS Update");
                        } else {
                            mJobStatusViewDestinationArrived.setStatusDescription(DateTimeUtils.getDisplayJobStatus(
                                    DateTimeUtils.parse(statusHistory.getServerTimeUtc()), mTimeZone).trim() + " "
                                    + getString(R.string.dot) + " " + statusHistory.getUserName());
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (Exception e) {
            mHomeActivity.mintLogException(e);
        }
    }

    @Override
    public void onSave() {
        getActivity().onBackPressed();
    }

    @OnClick({R.id.job_status_view_en_route, R.id.job_status_view_on_scene, R.id.job_status_view_tow_progress,
            R.id.job_status_view_destination_arrived, R.id.job_status_view_job_completed})
    public void onViewClicked(View view) {
        if (isEditable && isClickable) {
            if (Utils.isNetworkAvailable()) {
                isClickable = false;
                switch (view.getId()) {
                    case R.id.job_status_view_en_route:
                        statusDescription = JOB_STATUS_EN_ROUTE;
                        break;
                    case R.id.job_status_view_on_scene:
                        statusDescription = JOB_STATUS_ON_SCENE;
                        break;
                    case R.id.job_status_view_tow_progress:
                        statusDescription = JOB_STATUS_TOWING;
                        break;
                    case R.id.job_status_view_destination_arrived:
                        statusDescription = JOB_STATUS_DESTINATION_ARRIVAL;
                        break;
                    case R.id.job_status_view_job_completed:
                        statusDescription = JOB_STATUS_JOB_COMPLETED;
                        break;
                }

                if (statusHistory != null && !TextUtils.isEmpty(statusHistory.getStatusCode())) {
//                    if (JOB_STATUS_ON_SCENE.equalsIgnoreCase(statusDescription)) {
//                        checkCriteriaMatched();
//                    } else {
                        updateSelectedStatus();
      //              }
                }
            } else {
                UserError mUserError = new UserError();
                mUserError.title = "";
                mUserError.message = getString(R.string.network_error_message);
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            }
        }
    }

    boolean isCriteriaMatched = false;

    private void checkCriteriaMatched() {
        String userId = mPrefs.getString(NccConstants.SIGNIN_USER_ID, "");
        isCriteriaMatched = false;
        if (userId.equalsIgnoreCase(mCurrentJob.getDispatchAssignedToId())) {
            checkCriteriaMatchedForMyJob();
        } else {
            checkCriteriaMatchedForOthersJob();
        }
    }

    private void checkCriteriaMatchedForMyJob() {
        if (!isPermissionDenied() && NCCApplication.mLocationData != null) {
            com.agero.ncc.model.Location currentLocation = new com.agero.ncc.model.Location(NCCApplication.mLocationData.getLatitude(), NCCApplication.mLocationData.getLongitude());
            checkDistanceForCriteriaMatch(currentLocation);
        } else {
            isCriteriaMatched = false;
            UserError userError = new UserError();
            userError.title = "";
            userError.message = "GPS/Location Permission is not Enabled";
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), userError);
        }
        updateCriteriaMatched();
    }

    private void checkCriteriaMatchedForOthersJob() {
        {
            mLocationRef = FirebaseDatabase.getInstance()
                    .getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mCurrentJob.getDispatchAssignedToId());
            mLocationRef.addValueEventListener(mLocationValueEventListener);

        }
    }

    private DatabaseReference mLocationRef;

    private ValueEventListener mLocationValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            try {
                mLocationRef.removeEventListener(mLocationValueEventListener);
            } catch (Exception e) {

            }
            if (dataSnapshot != null && dataSnapshot.hasChildren() && isAdded()) {

                Profile profile = dataSnapshot.getValue(Profile.class);
                if (profile != null && profile.getOnDuty() != null && profile.getOnDuty()) {
                    checkDistanceForCriteriaMatch(profile.getLocation());
                } else {
                    UserError userError = new UserError();
                    userError.title = "";
                    userError.message = "driver is off duty.";
                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), userError);
                    isCriteriaMatched = false;
                }
            } else {
                isCriteriaMatched = false;
            }

            updateCriteriaMatched();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            isCriteriaMatched = false;
            updateCriteriaMatched();
        }
    };

    private void checkDistanceForCriteriaMatch(com.agero.ncc.model.Location currentLocation) {
        if (mCurrentJob != null && mCurrentJob.getDisablementLocation() != null && currentLocation != null) {
            Location disablementLocation = new Location("DisablementLocation");
            disablementLocation.setLatitude(mCurrentJob.getDisablementLocation().getLatitude());
            disablementLocation.setLongitude(mCurrentJob.getDisablementLocation().getLongitude());

            float distance = SparkLocationUpdateService.calculateDistance(currentLocation, disablementLocation);
            if (distance != 0f && distance < GEOFENCE_DISTANCE) {
                isCriteriaMatched = true;
            } else {
                UserError userError = new UserError();
                userError.title = "";
                userError.message = "location not matched with onscene location";
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), userError);
                isCriteriaMatched = false;
            }
        } else {
            UserError userError = new UserError();
            userError.title = "";
            userError.message = "location not matched with onscene location";
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), userError);
            isCriteriaMatched = false;
        }
    }

    private void updateCriteriaMatched() {
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference("ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber + "/disablementLocation");
                    RxFirebaseDatabase.setValue(myRef.child("isCriteriaMatched"), isCriteriaMatched).subscribe(() -> {
                        hideProgress();
                        if (isAdded()) {
                            updateSelectedStatus();
                        }
                    }, throwable -> {
                        hideProgress();
                    });
                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    public boolean isPermissionDenied() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return
                    ContextCompat.checkSelfPermission(mHomeActivity.getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_DENIED || !Utils.isGpsEnabled(getActivity());
        } else {
            return false;
        }
    }

    private void updateSelectedStatus() {

        if(currentStatus != null) {
            if (isUndo(currentStatus.getStatusCode(), statusDescription)) {
                if (statusHistoryList != null && statusHistoryList.size() > 0) {
                    statusHistoryList.remove(statusHistoryList.remove((statusHistoryList.size() - 1)));
                }
                String serverTimeUtc = DateTimeUtils.getUtcTimeFormat();
                updateUndoStatusAPI(statusDescription, serverTimeUtc, mDispatchNumber, statusHistoryList, mDispatchAssignId, mDispatchAssignName, mDriverAssignedById, mDriverAssignedByName, currentStatus.getStatusCode(), new StatusUpdateListener() {
                    @Override
                    public void onSuccess() {
                        isClickable = true;
                        Status currentStatus = new Status();
                        currentStatus.setStatusCode(statusDescription);
                        currentStatus.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                        currentStatus.setUserName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                        currentStatus.setStatusTime(DateTimeUtils.getUtcTimeFormat());
                        currentStatus.setServerTimeUtc(DateTimeUtils.getUtcTimeFormat());
                        currentStatus.setSource("FIREBASE");
                        currentStatus.setSubSource(NccConstants.SUB_SOURCE);
                        currentStatus.setDeviceId(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                        mCurrentJob.setCurrentStatus(currentStatus);
                        if (NccConstants.IS_GEOFENCE_ENABLED) {
                            SparkLocationUpdateService.checkJobToAddOrRemove(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), mCurrentJob);
                        }
                        updateCurrentStatusUI(statusDescription, serverTimeUtc, mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                    }

                    @Override
                    public void onFailure() {
                        isClickable = true;
                    }
                });
            } else {
                if (statusDescription != null) {
                    //updateCurrentStatus(statusDescription, mDispatchNumber, currentStatusDatabaseReference, statusHistoryDatabaseReference);
                    if (statusHistoryList == null) {
                        statusHistoryList = new ArrayList<>();
                    }
                    statusHistoryList.add(currentStatus);
                    String serverTimeUtc = DateTimeUtils.getUtcTimeFormat();
                    updateStatusAPI(statusDescription, serverTimeUtc, mDispatchNumber, statusHistoryList, mDispatchAssignId, mDispatchAssignName, mDriverAssignedById, mDriverAssignedByName, new StatusUpdateListener() {
                        @Override
                        public void onSuccess() {
                            isClickable = true;
                            Status currentStatus = new Status();
                            currentStatus.setStatusCode(statusDescription);
                            currentStatus.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                            currentStatus.setUserName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                            currentStatus.setStatusTime(DateTimeUtils.getUtcTimeFormat());
                            currentStatus.setServerTimeUtc(DateTimeUtils.getUtcTimeFormat());
                            currentStatus.setSource("FIREBASE");
                            currentStatus.setSubSource(NccConstants.SUB_SOURCE);
                            currentStatus.setDeviceId(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                            mCurrentJob.setCurrentStatus(currentStatus);
                            if (NccConstants.IS_GEOFENCE_ENABLED) {
                                SparkLocationUpdateService.checkJobToAddOrRemove(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), mCurrentJob);
                            }
                            updateCurrentStatusUI(statusDescription, serverTimeUtc, mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                        }

                        @Override
                        public void onFailure() {
                            isClickable = true;
                        }
                    });
                }
            }
        }
    }

    private boolean isUndo(String currentStatusDescription, String statusDescription) {


        return getStatusCodeNumberForStatus(currentStatusDescription) > getStatusCodeNumberForStatus(statusDescription);
    }

    private int getStatusCodeNumberForStatus(String statusCode) {
        int statusNumber = -1;
        switch (statusCode) {
            case JOB_STATUS_OFFERED:
                statusNumber = 0;
                break;
            case JOB_STATUS_ACCEPTED:
                statusNumber = 1;
                break;
            case JOB_STATUS_AWARDED:
                statusNumber = 2;
                break;
            case JOB_STATUS_UNASSIGNED:
                statusNumber = 3;
                break;
            case JOB_STATUS_ASSIGNED:
                statusNumber = 4;
                break;
            case JOB_STATUS_EN_ROUTE:
                statusNumber = 5;
                break;
            case JOB_STATUS_ON_SCENE:
                statusNumber = 6;
                break;
            case JOB_STATUS_TOWING:
                statusNumber = 7;
                break;
            case JOB_STATUS_DESTINATION_ARRIVAL:
                statusNumber = 8;
                break;
            case JOB_STATUS_JOB_COMPLETED:
                statusNumber = 9;
                break;
        }

        return statusNumber;
    }

    /*private void updateUndoStatus() {
        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")) && !TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, "")) && !TextUtils.isEmpty(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID)) && !TextUtils.isEmpty(DateTimeUtils.getUtcTimeFormat())) {
            showProgress();
            TokenManager.getInstance().validateToken(((BaseActivity) getActivity()), new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {

                        jobDatabaseReference.runTransaction(new Transaction.Handler() {
                            @Override
                            public Transaction.Result doTransaction(MutableData mutableData) {
                                try {
                                    JobDetail mCurrentJob = mutableData.getValue(JobDetail.class);
                                    if (mCurrentJob.getStatusHistory() != null && mCurrentJob.getStatusHistory().size() > 0) {
                                        int index = mCurrentJob.getStatusHistory().size() - 1;
                                        Status currentStatusForUpdate = mCurrentJob.getStatusHistory().get(index);

                                        currentStatusForUpdate.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                                        currentStatusForUpdate.setUserName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                                        currentStatusForUpdate.setStatusTime(DateTimeUtils.getUtcTimeFormat());
                                        currentStatusForUpdate.setServerTimeUtc(DateTimeUtils.getUtcTimeFormat());
                                        currentStatusForUpdate.setSource("FIREBASE");
                                        currentStatusForUpdate.setSubSource(NccConstants.SUB_SOURCE);
                                        currentStatusForUpdate.setDeviceId(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                                        mCurrentJob.setCurrentStatus(currentStatusForUpdate);

                                        mCurrentJob.getStatusHistory().remove(index);
                                        mutableData.setValue(mCurrentJob);

                                        HashMap<String, String> extraDatas = new HashMap<>();
                                        extraDatas.put("current_status", new Gson().toJson(currentStatusForUpdate));
                                        extraDatas.put("dispatch_id", mDispatchNumber);
                                        mHomeActivity.mintlogEventExtraData("Update Status", extraDatas);
                                        if (mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_JOB_COMPLETED)) {
                                            showJobCompleteDialog();
                                        }

                                        return Transaction.success(mutableData);
                                    } else {
                                        return Transaction.abort();
                                    }

                                } catch (Exception e) {
                                    return Transaction.abort();
                                }
                            }

                            @Override
                            public void onComplete(DatabaseError databaseError, boolean b,
                                                   DataSnapshot dataSnapshot) {
                                hideProgress();
                            }
                        });

                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    mHomeActivity.tokenRefreshFailed();
                }
            });
        }

    }*/


    private void updateUiStatus(String currentStatusCode, String serverTimeUtc, String userName) {
        mJobStatusViewAssigned.resetViews();
        mJobStatusViewEnRoute.resetViews();
        mJobStatusViewOnScene.resetViews();
        mJobStatusViewTowProgress.resetViews();
        mJobStatusViewDestinationArrived.resetViews();
        mJobStatusViewJobCompleted.resetViews();

//        updateJobHoldUi(false);
        if (!isTow) {
            mJobStatusViewTowProgress.setVisibility(View.GONE);
            mJobStatusViewDestinationArrived.setVisibility(View.GONE);
            mViewBorderTowStatus.setVisibility(View.GONE);
            mViewBorderTowDesstinationStatus.setVisibility(View.GONE);
        }

        try {
            switch (currentStatusCode) {
                case JOB_STATUS_ASSIGNED:
                    mJobStatusViewAssigned.setStatusAndDescriptionEnabled(true, true);
                    mJobStatusViewEnRoute.setStatusAndDescriptionEnabled(false, false);
                    mJobStatusViewOnScene.setStatusAndDescriptionEnabled(false, false);
                    mJobStatusViewTowProgress.setStatusAndDescriptionEnabled(false, false);
                    mJobStatusViewDestinationArrived.setStatusAndDescriptionEnabled(false, false);

                    mJobStatusViewEnRoute.setEnabled(true);
                    mJobStatusViewEnRoute.setStatusText(JOB_STATUS_EN_ROUTE);

                    mJobStatusViewAssigned.setStatusSelected(true);
                    mJobStatusViewAssigned.setStatusAndDescription(JOB_STATUS_ASSIGNED, DateTimeUtils.getDisplayJobStatus(
                            DateTimeUtils.parse(serverTimeUtc), mTimeZone).trim() + " "
                            + getString(R.string.dot) + " " + userName);
//                    updateJobHoldUi(false);
                    break;
                case JOB_STATUS_EN_ROUTE:
                    mJobStatusViewEnRoute.setStatusSelected(true);
                    mJobStatusViewEnRoute.setStatusAndDescription(JOB_STATUS_EN_ROUTE, DateTimeUtils.getDisplayJobStatus(
                            DateTimeUtils.parse(serverTimeUtc), mTimeZone).trim() + " "
                            + getString(R.string.dot) + " " + userName);

                    mJobStatusViewAssigned.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewEnRoute.setStatusAndDescriptionEnabled(true, true);
                    mJobStatusViewOnScene.setStatusAndDescriptionEnabled(true, false);
                    mJobStatusViewTowProgress.setStatusAndDescriptionEnabled(false, false);
                    mJobStatusViewDestinationArrived.setStatusAndDescriptionEnabled(false, false);

                    mJobStatusViewOnScene.setEnabled(true);
//                    updateJobHoldUi(false);
                    break;
                case JOB_STATUS_ON_SCENE:

                    if (isTow) {
                        mJobStatusViewOnScene.setStatusSelected(true);
                        mJobStatusViewOnScene.setStatusAndDescription(JOB_STATUS_ON_SCENE, DateTimeUtils.getDisplayJobStatus(
                                DateTimeUtils.parse(serverTimeUtc), mTimeZone).trim() + " "
                                + getString(R.string.dot) + " " + userName);

                        mJobStatusViewAssigned.setStatusAndDescriptionEnabled(false, true);
                        mJobStatusViewEnRoute.setStatusAndDescriptionEnabled(true, true);
                        mJobStatusViewOnScene.setStatusAndDescriptionEnabled(true, true);
                        mJobStatusViewTowProgress.setStatusAndDescriptionEnabled(true, false);
                        mJobStatusViewDestinationArrived.setStatusAndDescriptionEnabled(false, false);

                        mJobStatusViewEnRoute.setEnabled(true);
                        mJobStatusViewTowProgress.setEnabled(true);
                    } else {
                        mJobStatusViewOnScene.setStatusSelected(true);
                        mJobStatusViewOnScene.setStatusAndDescription(JOB_STATUS_ON_SCENE, DateTimeUtils.getDisplayJobStatus(
                                DateTimeUtils.parse(serverTimeUtc), mTimeZone).trim() + " "
                                + getString(R.string.dot) + " " + userName);

                        mJobStatusViewAssigned.setStatusAndDescriptionEnabled(false, true);
                        mJobStatusViewEnRoute.setStatusAndDescriptionEnabled(true, true);
                        mJobStatusViewOnScene.setStatusAndDescriptionEnabled(true, true);
                        mJobStatusViewEnRoute.setEnabled(true);
                        mJobStatusViewJobCompleted.setStatusAndDescriptionEnabled(true, true);
                        mJobStatusViewJobCompleted.setEnabled(true);

                    }

//                    updateJobHoldUi(true);
                    break;
                case JOB_STATUS_TOWING:
                    mJobStatusViewTowProgress.setStatusSelected(true);
                    mJobStatusViewTowProgress.setStatusAndDescription(JOB_STATUS_TOWING, DateTimeUtils.getDisplayJobStatus(
                            DateTimeUtils.parse(serverTimeUtc), mTimeZone).trim() + " "
                            + getString(R.string.dot) + " " + userName);

                    mJobStatusViewAssigned.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewEnRoute.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewOnScene.setStatusAndDescriptionEnabled(true, true);
                    mJobStatusViewTowProgress.setStatusAndDescriptionEnabled(true, true);
                    mJobStatusViewDestinationArrived.setStatusAndDescriptionEnabled(true, false);

                    mJobStatusViewOnScene.setEnabled(true);
                    mJobStatusViewDestinationArrived.setEnabled(true);
//                    updateJobHoldUi(true);
                    break;
                case JOB_STATUS_DESTINATION_ARRIVAL:
                    mJobStatusViewDestinationArrived.setStatusSelected(true);
                    mJobStatusViewDestinationArrived.setStatusAndDescription(JOB_STATUS_DESTINATION_ARRIVAL, DateTimeUtils.getDisplayJobStatus(
                            DateTimeUtils.parse(serverTimeUtc), mTimeZone).trim() + " "
                            + getString(R.string.dot) + " " + userName);

                    mJobStatusViewAssigned.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewEnRoute.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewOnScene.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewTowProgress.setStatusAndDescriptionEnabled(true, true);
                    mJobStatusViewDestinationArrived.setStatusAndDescriptionEnabled(true, true);
                    mJobStatusViewJobCompleted.setStatusAndDescriptionEnabled(true, true);

                    mJobStatusViewTowProgress.setEnabled(true);
                    mJobStatusViewJobCompleted.setEnabled(true);
//                    updateJobHoldUi(true);
                    break;
                case JOB_STATUS_JOB_COMPLETED:
                    mJobStatusViewJobCompleted.setStatusSelected(true);
                    mJobStatusViewJobCompleted.setStatusAndDescription(JOB_STATUS_JOB_COMPLETED, DateTimeUtils.getDisplayJobStatus(
                            DateTimeUtils.parse(serverTimeUtc), mTimeZone).trim() + " "
                            + getString(R.string.dot) + " " + userName);

                    mJobStatusViewAssigned.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewEnRoute.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewOnScene.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewTowProgress.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewDestinationArrived.setStatusAndDescriptionEnabled(false, true);
                    mJobStatusViewJobCompleted.setStatusAndDescriptionEnabled(true, true);

                    mJobStatusViewDestinationArrived.setEnabled(false);
                    mJobStatusViewDestinationArrived.setEnabled(true);
//                    updateJobHoldUi(false);
                    break;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void updateUiForHistory() {
        mViewBorderCurrentStatus.setVisibility(View.GONE);
        mTextSelectCurrentStatus.setVisibility(View.GONE);
        mConstraintJobHold.setVisibility(View.GONE);
        mJobStatusViewAssigned.setStatusEnabled();
        mJobStatusViewEnRoute.setStatusEnabled();
        mJobStatusViewOnScene.setStatusEnabled();
        mJobStatusViewTowProgress.setStatusEnabled();
        mJobStatusViewDestinationArrived.setStatusEnabled();
        mJobStatusViewJobCompleted.setStatusEnabled();
        mJobStatusViewAssigned.setStatusSelected(false);
        mJobStatusViewEnRoute.setStatusSelected(false);
        mJobStatusViewOnScene.setStatusSelected(false);
        mJobStatusViewTowProgress.setStatusSelected(false);
        mJobStatusViewDestinationArrived.setStatusSelected(false);
        mJobStatusViewJobCompleted.setStatusSelected(false);
        if (!isTow) {
            mJobStatusViewTowProgress.setVisibility(View.GONE);
            mJobStatusViewDestinationArrived.setVisibility(View.GONE);
            mViewBorderTowStatus.setVisibility(View.GONE);
            mViewBorderTowDesstinationStatus.setVisibility(View.GONE);
        }
    }

   /* private void updateJobHoldUi(boolean isEnabled) {
        if (isEnabled) {
            mTextJobStatusJobHold.setEnabled(true);
            mSwitchJobHold.setEnabled(true);
        } else {
            mTextJobStatusJobHold.setEnabled(false);
            mSwitchJobHold.setEnabled(false);
        }
        mSwitchJobHold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (mSwitchJobHold.isPressed()) {
                    updateJobHoldFirebase(b);
                }
            }
        });
    }*/

    private void updateJobHoldFirebase(boolean value) {
        enableDisableStaus(value);
        RxFirebaseDatabase.setValue(currentStatusDatabaseReference.child("isJobOnHold"), value).subscribe(() -> {
            hideProgress();
        }, throwable -> {
            if (isAdded()) {
                mHomeActivity.mintlogEvent("Job Status Job Hold Firebase Database Error - " + throwable.getLocalizedMessage());
            }
            hideProgress();
        });
    }

    private void enableDisableStaus(boolean value) {
        updateUiStatus(currentStatus.getStatusCode(), currentStatus.getServerTimeUtc(), currentStatus.getUserName());
        if (value) {
            mTextReasonJobOnHold.setVisibility(View.VISIBLE);
            mTextReasonDescription.setVisibility(View.VISIBLE);
            mViewBorderStatusDescription.setVisibility(View.VISIBLE);
            updateUiForJobHold(true);
        } else {
            mTextReasonJobOnHold.setVisibility(View.GONE);
            mTextReasonDescription.setVisibility(View.GONE);
            mViewBorderStatusDescription.setVisibility(View.GONE);
            updateUiForJobHold(false);
        }
    }

    private void updateUiForJobHold(boolean isJobHold) {
        mJobStatusViewAssigned.setIsDisable(isJobHold);
        mJobStatusViewDestinationArrived.setIsDisable(isJobHold);
        mJobStatusViewEnRoute.setIsDisable(isJobHold);
        mJobStatusViewJobCompleted.setIsDisable(isJobHold);
        mJobStatusViewOnScene.setIsDisable(isJobHold);
        mJobStatusViewTowProgress.setIsDisable(isJobHold);
    }

    private String getStatusToUndo(String selectedStatus) {
        String statusCode = currentStatus.getStatusCode();
        if (statusCode.equals(JOB_STATUS_ON_SCENE) && selectedStatus.equals(JOB_STATUS_EN_ROUTE)) {
            return JOB_STATUS_ON_SCENE;
        } else if (statusCode.equals(JOB_STATUS_TOWING) && selectedStatus.equals(JOB_STATUS_ON_SCENE)) {
            return JOB_STATUS_TOWING;
        } else if (statusCode.equals(JOB_STATUS_DESTINATION_ARRIVAL) && selectedStatus.equals(JOB_STATUS_TOWING)) {
            return JOB_STATUS_DESTINATION_ARRIVAL;
        }
        return null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            if (currentStatusDatabaseReference != null) {
                currentStatusDatabaseReference.removeEventListener(currentStatusEventListener);
            }
            if (statusHistoryDatabaseReference != null) {
                statusHistoryDatabaseReference.removeEventListener(statusHistoryEventListener);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            if (currentStatusDatabaseReference != null) {
                currentStatusDatabaseReference.removeEventListener(currentStatusEventListener);
            }
            if (statusHistoryDatabaseReference != null) {
                statusHistoryDatabaseReference.removeEventListener(statusHistoryEventListener);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (currentStatusDatabaseReference != null) {
                currentStatusDatabaseReference.removeEventListener(currentStatusEventListener);
            }
            if (statusHistoryDatabaseReference != null) {
                statusHistoryDatabaseReference.removeEventListener(statusHistoryEventListener);
            }
        } catch (Exception e) {

        }
    }
}
