package com.agero.ncc.firebasefunctions;

import android.support.annotation.VisibleForTesting;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class Serializer {
    @VisibleForTesting
    static final String LONG_TYPE = "type.googleapis.com/google.protobuf.Int64Value";
    @VisibleForTesting
    static final String UNSIGNED_LONG_TYPE = "type.googleapis.com/google.protobuf.UInt64Value";
    private final DateFormat dateFormat;

    public Serializer() {
        this.dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        this.dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public Object encode(Object obj) {
        if (obj != null && obj != JSONObject.NULL) {
            JSONObject result;
            if (obj instanceof Long) {
                result = new JSONObject();

                try {
                    result.put("@type", "type.googleapis.com/google.protobuf.Int64Value");
                    result.put("value", obj.toString());
                    return result;
                } catch (JSONException var9) {
                    throw new RuntimeException("Error encoding Long.", var9);
                }
            } else if (obj instanceof Number) {
                return obj;
            } else if (obj instanceof String) {
                return obj;
            } else if (obj instanceof Boolean) {
                return obj;
            } else if (obj instanceof JSONObject) {
                return obj;
            } else if (obj instanceof JSONArray) {
                return obj;
            } else {
                Object o;
                String key;
                Object value;
                Iterator keys;
                if (obj instanceof Map) {
                    result = new JSONObject();
                    Map<?, ?> m = (Map)obj;
                    keys = m.keySet().iterator();

                    while(keys.hasNext()) {
                        o = keys.next();
                        if (!(o instanceof String)) {
                            throw new IllegalArgumentException("Object keys must be strings.");
                        }

                        key = (String)o;
                        value = this.encode(m.get(o));

                        try {
                            result.put(key, value);
                        } catch (JSONException var10) {
                            throw new RuntimeException(var10);
                        }
                    }

                    return result;
                } else {
                    JSONArray result1;
                    if (obj instanceof List) {
                        result1 = new JSONArray();
                        List<?> l = (List)obj;
                        keys = l.iterator();

                        while(keys.hasNext()) {
                            o = keys.next();
                            result1.put(this.encode(o));
                        }

                        return result1;
                    } else if (obj instanceof JSONObject) {
                        result = new JSONObject();
                        JSONObject m = (JSONObject)obj;
                        keys = m.keys();

                        while(keys.hasNext()) {
                            String k = (String)keys.next();
                            if (!(k instanceof String)) {
                                throw new IllegalArgumentException("Object keys must be strings.");
                            }

                            key = k;
                            value = this.encode(m.opt(k));

                            try {
                                result.put(key, value);
                            } catch (JSONException var11) {
                                throw new RuntimeException(var11);
                            }
                        }

                        return result;
                    } else if (!(obj instanceof JSONArray)) {
                        throw new IllegalArgumentException("Object cannot be encoded in JSON: " + obj);
                    } else {
                        result1 = new JSONArray();
                        JSONArray l = (JSONArray)obj;

                        for(int i = 0; i < l.length(); ++i) {
                            o = l.opt(i);
                            result1.put(this.encode(o));
                        }

                        return result1;
                    }
                }
            }
        } else {
            return JSONObject.NULL;
        }
    }

    public Object decode(Object obj) {
        if (obj instanceof Number) {
            return obj;
        } else if (obj instanceof String) {
            return obj;
        } else if (obj instanceof Boolean) {
            return obj;
        } else if (obj instanceof JSONObject) {
            if (((JSONObject)obj).has("@type")) {
                String type = ((JSONObject)obj).optString("@type");
                String value = ((JSONObject)obj).optString("value");
                if (type.equals("type.googleapis.com/google.protobuf.Int64Value")) {
                    try {
                        return Long.parseLong(value);
                    } catch (NumberFormatException var6) {
                        throw new IllegalArgumentException("Invalid Long format:" + value);
                    }
                }

                if (type.equals("type.googleapis.com/google.protobuf.UInt64Value")) {
                    try {
                        return Long.parseLong(value);
                    } catch (NumberFormatException var7) {
                        throw new IllegalArgumentException("Invalid Long format:" + value);
                    }
                }
            }

            Map<String, Object> result = new HashMap();
            Iterator keys = ((JSONObject)obj).keys();

            while(keys.hasNext()) {
                String key = (String)keys.next();
                Object value = this.decode(((JSONObject)obj).opt(key));
                result.put(key, value);
            }

            return result;
        } else if (!(obj instanceof JSONArray)) {
            if (obj == JSONObject.NULL) {
                return null;
            } else {
                throw new IllegalArgumentException("Object cannot be decoded from JSON: " + obj);
            }
        } else {
            List<Object> result = new ArrayList();

            for(int i = 0; i < ((JSONArray)obj).length(); ++i) {
                Object value = this.decode(((JSONArray)obj).opt(i));
                result.add(value);
            }

            return result;
        }
    }
}

