package com.agero.ncc.utils;

import android.text.TextUtils;

import com.agero.ncc.model.JobDetail;

import java.text.ParseException;
import java.util.Comparator;

public class DispatcherJobDetailsComparator implements Comparator<JobDetail> {

    private static final int DONT_SWAP_EQUAL = 0;
    private static final int SWAP_LESSER = -1;
    private static final int SWAP_GREATER= 1;

    @Override
    public int compare(JobDetail job1, JobDetail job2) {

        if (job1 != null && job1.getCurrentStatus() != null && job2 != null && job2.getCurrentStatus() != null && !TextUtils.isEmpty(job1.getCurrentStatus().getStatusCode()) && !TextUtils.isEmpty(job2.getCurrentStatus().getStatusCode())) {

            int sectionNumberJob1 = sectionNumberForJobStatus(job1.getCurrentStatus().getStatusCode());
            int sectionNumberJob2 = sectionNumberForJobStatus(job2.getCurrentStatus().getStatusCode());

            if (sectionNumberJob1 == sectionNumberJob2) {
                if(sectionNumberJob1 == 1) {
                    if (!TextUtils.isEmpty(job1.getDispatchCreatedDate()) && !TextUtils.isEmpty(job2.getDispatchCreatedDate())) {
                        try {
                            return DateTimeUtils.parse(job2.getDispatchCreatedDate()).compareTo(DateTimeUtils.parse(job1.getDispatchCreatedDate()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                            return DONT_SWAP_EQUAL;
                        }
                    } else {
                        return DONT_SWAP_EQUAL;
                    }
                }else{
                    if (job1.getQuotedEta() != null && job2.getQuotedEta() != null) {
                        return String.valueOf(job1.getQuotedEta()).compareTo(String.valueOf(job2.getQuotedEta()));
                    } else {
                        return DONT_SWAP_EQUAL;
                    }
                }
            } else if (sectionNumberJob1 > sectionNumberJob2) {
                return SWAP_GREATER;
            } else {
                return SWAP_LESSER;
            }
        } else {
            return DONT_SWAP_EQUAL;
        }
    }

    private int sectionNumberForJobStatus(String jobStatusCode) {
        int sectionNumber;
        switch (jobStatusCode) {
            case NccConstants.JOB_STATUS_OFFERED:
            case NccConstants.JOB_STATUS_ACCEPTED:
            case NccConstants.JOB_STATUS_AWARDED:
                sectionNumber = 1;
                break;
            case NccConstants.JOB_STATUS_UNASSIGNED:
                sectionNumber = 2;
                break;
            default:
                sectionNumber = 3;
                break;
        }
        return sectionNumber;
    }
}
